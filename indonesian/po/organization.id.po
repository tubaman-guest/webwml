msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2010-11-28 23:20+0700\n"
"Last-Translator: Mahyuddin Susanto <udienz@gmail.com>\n"
"Language-Team: Debian Indonesia Translator <debian-l10n@debian-id.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Indonesian\n"
"X-Poedit-Country: INDONESIA\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "sekarang"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "anggota"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
#| msgid "Release Managers"
msgid "Stable Release Manager"
msgstr "Manager Rilis"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#: ../../english/intro/organization.data:39
#, fuzzy
#| msgid "chairman"
msgid "chair"
msgstr "ketua"

#: ../../english/intro/organization.data:42
#, fuzzy
#| msgid "FTP Assistants"
msgid "assistant"
msgstr "Asisten FTP"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "sekretaris"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "Petugas"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "Distribusi"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Publikasi"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr "Dukungan dan infrastruktur"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "Ketua"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "Komisi Teknis"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "Sekretaris"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "Proyek Pengembangan"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "Arsip FTP"

#: ../../english/intro/organization.data:96
#, fuzzy
#| msgid "FTP Master"
msgid "FTP Masters"
msgstr "FTP Master"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "Asisten FTP"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "Paket individu"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "Menagement rilis"

#: ../../english/intro/organization.data:120
#, fuzzy
#| msgid "Release Notes"
msgid "Release Team"
msgstr "Catatan Rilis"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "Jaminan Kualitas"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "Tim Instalasi Sistem"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "Catatan Rilis"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "CD Image"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "Produksi"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "Pengujian"

#: ../../english/intro/organization.data:149
#, fuzzy
#| msgid "Support and Infrastructure"
msgid "Autobuilding infrastructure"
msgstr "Dukungan dan infrastruktur"

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:159
#, fuzzy
#| msgid "System Administration"
msgid "Buildd administration"
msgstr "Administrasi sistem"

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "Dokumentasi"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr "Daftar paket yang butuh dikerjakan dan paket yang diharapkan"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "Ports"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "Konfigurasi Instimewa"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "Laptop"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "Firewall"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "Sistem embedded"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "Kontak press"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "Halaman web"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:263
#, fuzzy
#| msgid "Development Projects"
msgid "Debian Women Project"
msgstr "Proyek Pengembangan"

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "Acara"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Komisi Teknis"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr "Program Partner"

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr "Koordiasi donasi perangkat keras"

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "Dukungan pengguna"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr "Sistem Bug Tracking"

#: ../../english/intro/organization.data:379
#, fuzzy
#| msgid "Mailing Lists Administration"
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Asministrasi Milis"

#: ../../english/intro/organization.data:387
#, fuzzy
#| msgid "New Maintainers Front Desk"
msgid "New Members Front Desk"
msgstr "New Maintainers Front Desk"

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr "Pengelola akun Debian"

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Pengelola Keyring (PGP dan GPG)"

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "Tim keamanan"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "Halaman konsultan"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "Halaman penyedia CD"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "Peraturan"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "Administrasi sistem"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Ini adalah alamat yang digunakan ketika menemui satu masalah dalam mesin "
"Debian, termasuk masalah kata sandi atau anda membutuhkan instalasi paket"

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Jika anda mempunyai masalah di mesin Debian, silakan lihat halaman <a href="
"\"https://db.debian.org/machines.cgi\">mesin Debian</a>, yang mengandung "
"informasi administrasi per mesin"

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator Direktori LDAP pengembang"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "Cermin"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "Pengelola DNS"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "Sistem tracking paket"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Administrator Alioth"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr "Debian untuk anak kecil dari 1 sampai 99"

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr "Debian untuk medis dan penelitian"

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr "Debian untuk edukasi"

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr "Debian di kantor hukum"

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr "Debian untuk penyandang cacat"

#: ../../english/intro/organization.data:486
#, fuzzy
#| msgid "Debian for medical practice and research"
msgid "Debian for science and related research"
msgstr "Debian untuk medis dan penelitian"

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian untuk edukasi"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Tim Instalasi Sistem"

#~ msgid "Publicity"
#~ msgstr "Publikasi"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Ini belum menjadi Proyek internal Debian tapi sudah diumumkan akan "
#~ "disertakan "

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribusi Debian Multimedia"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux untuk Komputasi Enterprise"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian untuk organisasi nirlaba"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Sistem operasi universal sebagai desktop anda"

#~ msgid "Accountant"
#~ msgstr "Akuntan"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinasi penandatanganan kunci"

#~ msgid "Security Testing Team"
#~ msgstr "Tim penguji keamanan"

#~ msgid "Mailing List Archives"
#~ msgstr "Arsip Milis"

#~ msgid "Handhelds"
#~ msgstr "Handhelds"

#~ msgid "APT Team"
#~ msgstr "Tim APT"

#~ msgid "Vendors"
#~ msgstr "Penyedia"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Manager rilis untuk ``stable''"

#~ msgid "Release Assistants"
#~ msgstr "Asisten Rilis"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribusi Debian Racikan"

#~ msgid "Security Audit Project"
#~ msgstr "Proyek audit keamanan"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Tim keamanan"

#~ msgid "Alioth administrators"
#~ msgstr "Administrator Alioth"
