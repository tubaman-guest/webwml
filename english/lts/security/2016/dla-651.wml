<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various security issues were found and fixed in graphicsmagick in Debian
wheezy LTS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7446">CVE-2016-7446</a>

    <p>Heap buffer overflow issue in MVG/SVG rendering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7447">CVE-2016-7447</a>

    <p>Heap overflow of the EscapeParenthesis() function</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7449">CVE-2016-7449</a>

    <p>TIFF related problems due to use of strlcpy use.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7800">CVE-2016-7800</a>

    <p>Fix unsigned underflow leading to heap overflow when
    parsing 8BIM chunk.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u4.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-651.data"
# $Id: $
