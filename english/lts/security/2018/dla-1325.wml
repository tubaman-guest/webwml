<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jasper Mattsson found a remote code execution vulnerability in the
Drupal content management system. This potentially allows attackers to
exploit multiple attack vectors on a Drupal site, which could result in
the site being completely compromised.</p>

<p>For further information please refer to the official upstream advisory
at <a href="https://www.drupal.org/sa-core-2018-002.">https://www.drupal.org/sa-core-2018-002.</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.14-2+deb7u18.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1325.data"
# $Id: $
