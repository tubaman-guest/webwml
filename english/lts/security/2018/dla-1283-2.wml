<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>This is an update to DLA-1283-1. In DLA-1283-1 it is claimed that the issue
described in <a href="https://security-tracker.debian.org/tracker/CVE-2018-6594">CVE-2018-6594</a> is fixed. It turns out that the fix is partial and
upstream has decided not to fix the issue as it would break compatibility and
that ElGamal encryption was not intended to work on its own.</p>

<p>The recommendation is still to upgrade python-crypto packages. In addition
please take into account that the fix is not complete. If you have an
application using python-crypto is implementing ElGamal encryption you should
consider changing to some other encryption method.</p>

<p>There will be no further update to python-crypto for this specific CVE. A fix
would break compatibility, the problem has been ignored by regular Debian
Security team due to its minor nature and in addition to that we are close to
the end of life of the Wheezy security support.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6594">CVE-2018-6594</a>

<p>python-crypto generated weak ElGamal key parameters, which allowed attackers to
obtain sensitive information by reading ciphertext data (i.e., it did not have
semantic security in face of a ciphertext-only attack).</p></li>

</ul>

<p>We recommend that you upgrade your python-crypto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in python-crypto version 2.6-4+deb7u8</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1283-2.data"
# $Id: $
