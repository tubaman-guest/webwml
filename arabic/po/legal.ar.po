# translation of legal.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
# Mohamed Amine <med@mailoo.org>, 2013.
msgid ""
msgstr ""
"PO-Revision-Date: 2013-05-08 16:26+0000\n"
"Last-Translator: Mohamed Amine <med@mailoo.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "معلومات الرخصة"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr ""

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "أسئلة DFSG الشائعة"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr ""

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr ""

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr ""

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "تاريخ النشر"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "الرخصة"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "الإصدار"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "الملخّص"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr ""

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "النقاش"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "الخلاصة الأصلية"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"الخلاصة الأصلية لـ <summary-author/> يمكن إيجادها في <a href=\"<summary-url/>"
"\">قائمة الأراشيف</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "أعدّت هذه الخلاصة من قِبَل <summary-author/>"

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "نص الرخصة (مترجم)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "نص الرخصة"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "حُر"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "غير حُر"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr ""

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "حُر"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "غير حُر"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr ""

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
