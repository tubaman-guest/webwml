# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2018-01-26 22:38+0900\n"
"Last-Translator: 세벌 <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "파일"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "패키지"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "스코어"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "번역자"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "팀"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "날짜"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "상태"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "문자열"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "버그"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, <get-var country />에서 말함"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "모르는 언어"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "이 페이지는 <get-var date />에 모은 데이터로 만들어졌습니다."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr "이 파일에서 작업하기 전에, 그것들이 최신인지 확인하십시오!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "섹션: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "지역화"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "언어 목록"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "순위"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "힌트"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "오류"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "POT 파일"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "번역자를 위한 힌트"
