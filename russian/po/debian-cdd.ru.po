# translation of debian-cdd.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml debian-cdd\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-11-17 19:10+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/debian-cdd.wml:28
#: ../../english/template/debian/debian-cdd.wml:68
msgid "Debian-Med"
msgstr "Debian в медицине (Debian-Med)"

#: ../../english/template/debian/debian-cdd.wml:31
#: ../../english/template/debian/debian-cdd.wml:72
msgid "Debian-Jr"
msgstr "Debian для детей (Debian-Jr)"

#: ../../english/template/debian/debian-cdd.wml:34
#: ../../english/template/debian/debian-cdd.wml:76
msgid "Debian-Lex"
msgstr "Debian в юриспруденции (Debian-Lex)"

#: ../../english/template/debian/debian-cdd.wml:36
msgid ""
"The list below includes various software projects which are of some interest "
"to the <get-var cdd-name /> Project. Currently, only a few of them are "
"available as Debian packages. It is our goal, however, to include all "
"software in <get-var cdd-name /> which can sensibly add to a high quality "
"Custom Debian Distribution."
msgstr ""
"В нижеприведённом списке перечислены различные проекты, находящиеся в сфере "
"интересов проекта <get-var cdd-name />. На данный момент лишь небольшая их "
"часть доступна в форме пакетов Debian. Нашей целью, тем не менее, является "
"включение в <get-var cdd-name /> всего этого программного обеспечения, чтобы "
"сделать его высококачественным адаптированным дистрибутивом Debian."

#: ../../english/template/debian/debian-cdd.wml:41
msgid ""
"For a better overview of the project's availability as a Debian package, "
"each head row has a color code according to this scheme:"
msgstr ""
"Чтобы было лучше видно, какие программные средства доступны в форме пакетов "
"Debian, цвет каждой строки-заголовка выбран в соответствии со следующей "
"схемой:"

#: ../../english/template/debian/debian-cdd.wml:47
msgid ""
"Green: The project is <a href=\"<get-var url />\">available as an official "
"Debian package</a>"
msgstr ""
"Зелёный: программа включена в <a href=\"<get-var url />\">официальный пакет "
"Debian</a>"

#: ../../english/template/debian/debian-cdd.wml:54
msgid ""
"Yellow: The project is <a href=\"<get-var url />\">available as an "
"unofficial Debian package</a>"
msgstr ""
"Жёлтый: программа включена в <a href=\"<get-var url />\">неофициальный пакет "
"Debian</a>"

#: ../../english/template/debian/debian-cdd.wml:61
msgid ""
"Red: The project is <a href=\"<get-var url />\">not (yet) available as a "
"Debian package</a>"
msgstr ""
"Красный: программа <a href=\"<get-var url />\">недоступна (пока) в форме "
"пакета Debian</a>"

#: ../../english/template/debian/debian-cdd.wml:79
msgid ""
"If you discover a project which looks like a good candidate for <get-var cdd-"
"name /> to you, or if you have prepared an unofficial Debian package, please "
"do not hesitate to send a description of that project to the <a href=\"<get-"
"var cdd-list-url />\"><get-var cdd-name /> mailing list</a>."
msgstr ""
"Если вы обнаружили проект, который, на ваш взгляд, должен входить в "
"дистрибутив <get-var cdd-name />, или если вы создали неофициальный пакет "
"Debian, не стесняйтесь прислать описание проекта в <a href=\"<get-var cdd-"
"list-url />\"><get-var cdd-name />список рассылки</a>."

#: ../../english/template/debian/debian-cdd.wml:84
msgid "Official Debian packages"
msgstr "Официальные пакеты Debian"

#: ../../english/template/debian/debian-cdd.wml:88
msgid "Unofficial Debian packages"
msgstr "Неофициальные пакеты Debian"

#: ../../english/template/debian/debian-cdd.wml:92
msgid "Debian packages not available"
msgstr "Пакеты Debian недоступны"

#: ../../english/template/debian/debian-cdd.wml:96
msgid "There are no projects which fall into this category."
msgstr "Нет проектов, попадающих в эту категорию"

#: ../../english/template/debian/debian-cdd.wml:100
msgid "No homepage known"
msgstr "Нет данных о домашней странице"

#: ../../english/template/debian/debian-cdd.wml:104
msgid "License:"
msgstr "Лицензия:"

#: ../../english/template/debian/debian-cdd.wml:108
msgid "Free"
msgstr "Свободная"

#: ../../english/template/debian/debian-cdd.wml:112
msgid "Non-Free"
msgstr "Несвободная"

#: ../../english/template/debian/debian-cdd.wml:116
msgid "Unknown"
msgstr "Неизвестно"

#: ../../english/template/debian/debian-cdd.wml:120
msgid "Official Debian package"
msgstr "Официальный пакет Debian"

#: ../../english/template/debian/debian-cdd.wml:124
msgid "Unofficial Debian package"
msgstr "Неофициальный пакет Debian"

#: ../../english/template/debian/debian-cdd.wml:128
msgid "Debian package not available"
msgstr "Пакет Debian недоступен"

